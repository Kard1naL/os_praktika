﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Timers;

namespace Client
{
    class Program
    {
        static TcpClient client = null;
        const int port = 8888;
        const string address = "127.0.0.1";

        static NetworkStream Nwstream;

        static public void SendtoServer()
        {
            while (true)
            {
                try
                {
                    double randomNumber = Math.Clamp(Math.Round(2.0 + 1.0 * new Random().NextDouble(), 1), 2.0, 3.0);
                    Console.WriteLine("Клиент: {0}", randomNumber);
                    string message = randomNumber.ToString();
                    byte[] data = Encoding.Unicode.GetBytes(message);

                    Nwstream.Write(data, 0, data.Length);

                    Thread.Sleep(2700);
                }
                catch
                {
                    Console.WriteLine("Ошибка");
                    break;
                }
            }
        }

        static void Main(string[] args)
        {

            try
            {
                client = new TcpClient(address, port);
                Nwstream = client.GetStream();

                Console.Write("Имя: Клиент");
                Console.WriteLine();

                new Thread(new ThreadStart(SendtoServer)).Start();

                while (true)
                {

                    var data = new byte[64];
                    StringBuilder Strbuilder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = Nwstream.Read(data, 0, data.Length);
                        Strbuilder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (Nwstream.DataAvailable);

                    var message = Strbuilder.ToString();
                    Console.WriteLine("Сервер: {0}", message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                client.Close();
            }
        }


    }
}
