﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class Connect
    {

        public Dictionary<string, Task<byte[]>> TasksAndRequests;
        public int id;
        public TcpClient client;
        public Connect(TcpClient tcpClient, Dictionary<string, Task<byte[]>> Slovarik, int Id)
        {
            client = tcpClient;
            TasksAndRequests = Slovarik;
            id = Id;
            Console.WriteLine("Клиент #{0} подключился", Id);
        }

        public void Ssend(byte[] Answer, NetworkStream Stream)
        {
            try
            {
                Stream.Write(Answer, 0, Answer.Length);
            }
            catch
            {

            }
        }

        public byte[] Wworking(string numString)
        {
            double numDouble;
            if (!double.TryParse(numString, out numDouble))
            {
                Console.WriteLine("Ошибка");
            }
            Thread.Sleep(6500);
            TasksAndRequests.Remove(numString);
            return Encoding.Unicode.GetBytes((Math.Round(numDouble)).ToString());
        }


        public void Operation()
        {
            Task<byte[]> DefaultTask = null;
            NetworkStream stream = null;
            bool inwork = false;
            try
            {
                stream = client.GetStream();
                byte[] data = new byte[64];
                while (true)
                {
                 
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    Console.WriteLine("Клиент {0}: {1} => {2}", id, message, DateTime.Now.ToLongTimeString());
                    if (inwork == true)
                      
                    {
                        Ssend(Encoding.Unicode.GetBytes("Ожидаем"), stream);
                        continue;

                    }
                    if (TasksAndRequests.ContainsKey(message))
                    {
                        
                        DefaultTask = TasksAndRequests[message];
                        inwork = true;  
                    }
                    else
                    {
                        if (DefaultTask == null || inwork == false)
                        {
                           
                            TasksAndRequests.Add(message, Task.Factory.StartNew(() => Wworking(message)));
                            DefaultTask = TasksAndRequests[message];
                            inwork = true;
                        }


                    }
                    DefaultTask.ContinueWith(Task => {
                        inwork = false;
                        var answer = DefaultTask.Result;
                        Ssend(answer, stream);

                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Клиент #{id} отключился" );
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                if (client != null)
                    client.Close();
            }
        }
    }
}
